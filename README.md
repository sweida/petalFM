## PetalFM
基于react构建的简易精美的FM

[![react](https://img.shields.io/badge/react-v16.2.0-blue.svg?longCache=true)](https://facebook.github.io/react/)
[![react-router](https://img.shields.io/badge/react--router-v4.2.2-blue.svg?longCache=true)](https://reacttraining.com/react-router/)
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg?style=flat)](http://standardjs.com/)
[![Dependency Status](https://david-dm.org/alex1504/PetalFM/status.svg)](https://david-dm.org/alex1504/PetalFM)
[![MIT Licensed](https://img.shields.io/badge/License-MIT-blue.svg?style=flat)](https://opensource.org/licenses/MIT)

## 预览
以下预览图截取环境是iPhone8P微信内置浏览器

![preview](https://github.com/alex1504/PetalFM/raw/master/media/preview_1.jpg)
![preview](https://github.com/alex1504/PetalFM/raw/master/media/preview_2.jpg)
![preview](https://github.com/alex1504/PetalFM/raw/master/media/preview_3.jpg)
![preview](https://github.com/alex1504/PetalFM/raw/master/media/preview_4.jpg)

### 在线地址
访问 http://fm.huzerui.com/, 更好的体验效果请在移动端体验，扫描下面二维码。

![qrcode](https://github.com/alex1504/PetalFM/raw/master/media/qrcode.png)

### 测试账号
- 用户名: petalFM
- 密码: petalFM

## 技术栈
![based on](https://github.com/alex1504/PetalFM/raw/master/media/main-based-on.png)

-  Framework: [react](https://facebook.github.io/react/)
-  State Management: [redux](https://redux.js.org/)
-  Bundler: [Webpack](http://webpack.github.io/docs/)，[Babel](https://babeljs.io)
-  Language: [ES2015](https://babeljs.io/docs/learn-es2015/), [Less](http://lesscss.org/)
-  Library:
  - [React Router V4](https://reacttraining.com/react-router/)
  - [Material-UI](https://material-ui-next.com/)
- Lint: [ESLint](http://eslint.org/)
- Icon Support: [Iconfont](http://www.iconfont.cn)

## 特点
* 材质设计风格界面。主色调为粉色色调。
* 使用Leancloud提供数据服务。
* 专属FM和心动FM定制

## 功能

- [x]登录、注册和注销
- [x]定制个人偏好标签
- [x]精选调频，私人调频，红心调频频道选择
- [x]搜索歌曲，收集歌曲，下载歌曲
- [x]将歌曲添加到垃圾箱或从中回收歌曲
- []个人配置、话题切换等。

## 项目目录说明
```
.
|-- config                           // webpack配置目录
|-- database                         // 数据库备份目录
|-- media                            // readme描述资源
|-- src                              // 源码目录
|   |-- components                   // 公共组件
|   |-- pages                        // 页面组件
|       |-- Login                    // 登陆页面
|       |-- Guide                    // 标签定制页
|       |-- Index                    // 主页
|       |-- Search                   // 搜索页
|       |-- Admin                    // 后台数据录入页（仅管理员可见）
|       |-- Dislike                  // 用户不兴趣歌曲页
|   |-- router                       // 路由定义
|   |-- services                     // 后端服务目录
|       |-- avinit.js                // leancloudSDK初始化
|       |-- config.js                // 数据库相关配置
|       |-- songServices.js          // 歌曲相关服务
|       |-- userServices.js          // 用户相关服务
|       |-- index.js                 // 后端数据库服务入口
|   |-- store                        // 状态管理目录
|       |-- index.js                 // 加载各种store模块
|       |-- types                    // 定义触发状态改变类型
|       |-- reducers                 // 状态生成器
|   |-- Utils                        // 工具函数
|   |-- index.js                     // 程序入口文件
|-- .gitignore                       // Git提交忽略文件规则
|-- LICENSE                          // 授权协议
|-- README.md                        // 项目说明
|-- package.json                     // 配置项目相关信息
.
```

## 本地运行或二次开发
由于后端云开启了WEB安全域名，本地克隆项目后无法直接运行，解决方式如下：

前置工作：
- Step1：执行命令克隆项目到本地`git clone https://github.com/alex1504/PetalFM.git`
- Step2：安装依赖 `npm install`
- Step3：在 [https://leancloud.cn/](https://leancloud.cn/) 注册账号并且在后台创建一个leancloud应用
- Step4：在leancloud应用后台导入根目录database下面的数据库，并且在_User表创建你的管理员账号（用户名和密码需为4-8位的数字、字母或_）
- Step5：修改 `/src/services/config.js`配置如下：

```javascript
export const APP_ID = 'YOUR APP_ID FOUND IN LEANCLOUD APP SETTING';
export const APP_KEY = 'YOUR APP_KEY FOUND IN LEANCLOUD APP SETTING';
export const SUPER_USER_OBJECT_ID = 'YOUR SUPERUSER ACCOUNT OBJECT ID';
```
只有管理员才能看到Admin入口并且通过搜索接口录入歌曲，并设置歌曲所属类别。

开发：
- Step6：执行 `npm run dev`运行项目
- Step7：执行 `npm run build`打包项目，`/dist/`文件夹为打包后的项目文件

部署：
- Step9：在开发过程中，webpack-dev-server将本地 `/api/`请求转发到`http://music.163.com/api`（若需增加别的转发机制请修改package.json中的proxy配置，完整配置参考[http-proxy](https://github.com/nodejitsu/node-http-proxy#options)），因此部署到服务器时请借助nginx或nodejs服务器进行接口转发，否则搜索和录入服务不生效。

## 更多
- 反馈问题: [新建](https://github.com/alex1504/petalFM/issues/new)
- 联系: 邮件至: <a href="mailto:me@huzerui.com">me@huzerui.com</a>

## 协议
MIT © [alex1504](https://github.com/alex1504)